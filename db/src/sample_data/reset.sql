BEGIN;
\set QUIET on
\set ON_ERROR_STOP on
set client_min_messages to warning;
truncate data.todo restart identity cascade;
truncate data.user restart identity cascade;
truncate data.city restart identity cascade;
truncate data.high_school restart identity cascade;
truncate data.high_school_major restart identity cascade;
truncate data.university restart identity cascade;
truncate data.faculty restart identity cascade;
\ir data.sql
COMMIT;
