COPY koshnica_user (id, user_hash, gender, birth_date, city, abroad_city, primary_school_city, high_school_city, faculty_city, high_school, high_school_major, faculty, faculty_major, working_profession, abroad_faculty_city, abroad_faculty, abroad_faculty_major, qualification, working_city, abroad_working_city, languages) FROM STDIN (FREEZE ON, NULL '""');
1	xEEvsXsvpBNCcP5Df	Мушки	1/24/1993	491	""	491	491	""	1335	549	""	""	""	""	""	""	1147	491	""	{Енглески}
2	xEEvsXsvpBNCcP5Df	Женски	11/8/1997	491	""	491	491	""	1333	535	181	""	menadžer	""	""	""	2168	491	""	{Енглески}
3	xEEvsXsvpBNCcP5Df	Женски	1/10/1995	491	""	491	491	""	1335	550	3	23	Логопед	""	""	""	4191	491	""	{Енглески}
4	xEEvsXsvpBNCcP5Df	Мушки	11/29/1990	491	""	491	979	100	1414	143	""	""	Лингвиста преводилац	""	""	""	1147	491	""	{Енглески,Руски}
5	xEEvsXsvpBNCcP5Df	Мушки	6/9/1997	741	""	491	491	741	1333	536	31	196	??	""	""	""	4191	741	""	{Енглески}
6	xEEvsXsvpBNCcP5Df	Мушки	10/7/1997	100	""	491	491	100	1333	535	181	""	Video producent	""	""	""	4191	100	""	{Енглески}
7	xEEvsXsvpBNCcP5Df	Мушки	7/10/1997	741	""	491	491	741	1333	534	63	1735	Prirodno-matematički smer	""	""	""	4191	741	""	{Енглески}
8	xEEvsXsvpBNCcP5Df	Женски	5/9/1995	714	""	714	491	491	1334	539	136	""	Vaspitač	""	""	""	1000	714	""	{Енглески}
9	xEEvsXsvpBNCcP5Df	Мушки	12/8/1994	1014	""	491	491	1014	1334	539	60	1240	Пословни информатичар	""	""	""	4191	1014	""	{Енглески}
10	xEEvsXsvpBNCcP5Df	Мушки	1/24/1993	491	""	491	491	""	1335	549	""	""	Електромеханичар за термичке и расхладне уређаје	""	""	""	1147	491	""	{Енглески}
11	xEEvsXsvpBNCcP5Df	Женски	1/10/1995	491	""	491	491	100	1335	550	3	23	Лабораторијски техничар	""	""	""	4191	491	""	{Енглески}
12	xEEvsXsvpBNCcP5Df	Женски	12/31/1994	233	""	491	491	""	1335	550	""	""	Laboratorijski tehnicar	""	""	""	1836	233	""	{Енглески}
13	xEEvsXsvpBNCcP5Df	Женски	2/23/1994	741	""	491	491	""	1335	550	""	""	Laborant	""	""	""	1845	741	""	{Енглески}
14	xEEvsXsvpBNCcP5Df	Мушки	8/9/1992	491	""	491	491	""	1334	540	""	""	Радник обезбеђења	""	""	""	3977	491	""	{Енглески}
15	xEEvsXsvpBNCcP5Df	Женски	9/13/1994	491	""	491	491	1014	1334	541	60	1240	Економиста	""	""	""	4191	491	""	{Енглески}
16	xEEvsXsvpBNCcP5Df	Женски	9/12/1994	741	""	491	937	741	1340	566	65	1414	Лекар	""	""	""	4191	741	""	{Енглески}
17	xEEvsXsvpBNCcP5Df	Женски	12/6/1994	491	""	491	491	741	1334	538	62	1693	Finansijski administrator	""	""	""	4191	491	""	{Енглески}
18	xEEvsXsvpBNCcP5Df	Женски	3/29/1994	491	""	491	491	""	1334	538	""	""	Finansijski administrator	""	""	""	2468	491	""	{Енглески}
19	xEEvsXsvpBNCcP5Df	Женски	7/13/1995	741	""	491	491	741	1333	536	63	1732	Gimnazija	""	""	""	4191	741	""	{Енглески,Француски,Шпански}
20	xEEvsXsvpBNCcP5Df	Мушки	9/29/1992	1014	""	491	491	741	1335	548	7	1643	Profesor fizičke kulture	""	""	""	1134	1014	""	{Енглески,Мађарски,Шпански}
21	xEEvsXsvpBNCcP5Df	Мушки	4/26/1994	491	""	491	491	""	1335	548	""	""	Arhitektonki tehnicar	""	""	""	1531	491	""	{Енглески}
22	xEEvsXsvpBNCcP5Df	Мушки	10/24/1994	491	""	491	491	""	1335	551	""	""	Elektrotehnicar telekomunikacija	""	""	""	2830	491	""	{Енглески}
23	xEEvsXsvpBNCcP5Df	Женски	8/12/1994	491	""	491	491	741	1334	541	63	1734	ekonomista	""	""	""	4191	491	""	{Енглески}
24	xEEvsXsvpBNCcP5Df	Женски	3/22/1997	741	""	491	491	741	1333	535	172	""	Informatički smer	""	""	""	4191	741	""	{Енглески}
25	xEEvsXsvpBNCcP5Df	Мушки	12/26/1995	741	""	491	491	""	1335	552	""	""	Elektrotehnika	""	""	""	1130	741	""	{Енглески,Немачки}
26	xEEvsXsvpBNCcP5Df	Женски	12/5/1995	491	""	491	491	""	1335	553	""	""	Psiholog	New York	University of North Dakota	Psihologija	2468	491	""	{Енглески}
27	xEEvsXsvpBNCcP5Df	Женски	4/9/1994	491	""	491	491	1014	1333	535	""	""	Informatičar	""	""	""	1131	491	""	{Енглески}
28	xEEvsXsvpBNCcP5Df	Женски	5/29/1994	491	""	491	491	491	1335	550	136	""	Strukovni vaspitac	""	""	""	2017	491	""	{Енглески,Мађарски}
29	xEEvsXsvpBNCcP5Df	Женски	2/12/1996	741	""	491	491	741	1334	542	63	1740	Diplomirani sociolog	""	""	""	4191	741	""	{Енглески}
30	xEEvsXsvpBNCcP5Df	Женски	7/12/1992	491	""	491	491	1014	1334	539	60	1241	Ekonomista	""	""	""	1946	491	""	{Енглески,Немачки}
31	xEEvsXsvpBNCcP5Df	Женски	2/12/1996	741	""	491	491	741	1334	542	63	1740	Sociolog	""	""	""	4191	741	""	{Енглески}
32	xEEvsXsvpBNCcP5Df	Женски	11/8/1997	491	""	491	491	100	1333	535	181	""	Менаџер уметничке продукције и медија	""	""	""	4191	491	""	{Енглески}
33	xEEvsXsvpBNCcP5Df	Женски	6/27/1996	491	""	491	491	741	1334	539	62	1694	Ekonomski tehničar	""	""	""	4191	491	""	{Енглески}
34	xEEvsXsvpBNCcP5Df	Женски	9/19/1997	741	""	491	491	741	1333	536	65	1421	Društveno-jezički smer	""	""	""	4191	741	""	{Енглески,Немачки}
35	xEEvsXsvpBNCcP5Df	Мушки	7/10/1997	741	""	491	491	741	1333	534	63	1735	Prirodno-matematički smer	""	""	""	4191	741	""	{Енглески}
36	xEEvsXsvpBNCcP5Df	Женски	5/2/1997	""	New York	491	491	""	1333	534	""	""	Biznis Menadzer	New York	Pace University	Biznis Menadzment	4191	491	""	{Енглески}
37	xEEvsXsvpBNCcP5Df	Мушки	10/7/1997	100	""	491	491	100	1333	535	181	""	Diplomirani menadžer umetničke produkcije	""	""	""	1467	100	""	{Енглески}
38	xEEvsXsvpBNCcP5Df	Мушки	6/13/1997	741	""	491	741	741	1406	133	142	""	Струковни инжењер информационих технологија и система	""	""	""	1147	741	""	{Енглески}
39	xEEvsXsvpBNCcP5Df	Мушки	4/23/1997	741	""	491	491	741	1335	550	65	1416	Farmaceutski tehničar	""	""	""	1846	741	""	{Енглески,Немачки}
40	xEEvsXsvpBNCcP5Df	Женски	6/27/1994	491	""	491	491	741	1335	547	68	1583	Tehničar za zaštitu životne sredine	""	""	""	2556	491	""	{Енглески}
41	xEEvsXsvpBNCcP5Df	Мушки	4/22/1994	100	""	491	491	100	1335	552	5	462	Elektrotehnicar racunara	""	""	""	4191	100	""	{Енглески}
42	xEEvsXsvpBNCcP5Df	Мушки	1/30/1994	491	""	491	491	491	1335	551	""	""	Strukovni ekonomista	""	""	""	3412	491	""	{Енглески}
43	xEEvsXsvpBNCcP5Df	Мушки	3/6/1993	491	""	491	491	741	1335	551	170	1837	Електротехничар телекомуникација	""	""	""	4191	491	""	{Енглески,Шпански}
44	xEEvsXsvpBNCcP5Df	Женски	9/4/1994	741	""	491	491	741	1334	539	63	1736	Dipl.pedagog	""	""	""	2617	741	""	{Енглески}
45	xEEvsXsvpBNCcP5Df	Женски	1/27/1997	423	""	702	491	""	1335	550	""	""	Farmaceutski tehničar	""	""	""	1846	423	""	{Енглески}
46	xEEvsXsvpBNCcP5Df	Женски	5/21/1994	741	""	491	491	741	1335	550	68	1538	Лабораторијски техничар	""	""	""	4191	741	""	{Енглески,Турски}
47	xEEvsXsvpBNCcP5Df	Женски	7/11/1994	491	""	491	423	741	1890	145	86	40	diplomirani etnomuzikolog	""	""	""	975	491	""	{Енглески}
48	xEEvsXsvpBNCcP5Df	Женски	7/26/1994	741	""	491	491	741	1335	550	63	1733	Laboratorijski tehničar	""	""	""	4191	741	""	{Енглески}
49	xEEvsXsvpBNCcP5Df	Женски	10/18/1994	491	""	491	491	741	1335	550	65	1414	Doktor medicine	""	""	""	643	491	""	{Енглески}
50	xEEvsXsvpBNCcP5Df	Мушки	8/16/1994	741	""	491	491	423	1333	535	70	1275	Diploma iz srednje skole, zavrsen informaticki smer u gimnaziji	""	""	""	1121	741	""	{Енглески}
51	xEEvsXsvpBNCcP5Df	Женски	1/8/1993	491	""	491	491	""	1335	556	""	""	Tehnicar drumskog saobracaja	""	""	""	2290	491	""	{Енглески}
52	xEEvsXsvpBNCcP5Df	Мушки	2/16/1994	741	""	491	491	741	1333	536	142	""	Gimnazija -  društveno jezički smer	""	""	""	4191	741	""	{Енглески}
53	xEEvsXsvpBNCcP5Df	Мушки	12/12/1994	491	""	491	491	""	1335	552	""	""	Elektrotehnicar racunara	""	""	""	2598	491	""	{Енглески}
54	xEEvsXsvpBNCcP5Df	Женски	10/28/1996	741	""	491	491	741	1333	536	63	1731	Gimnazija	""	""	""	4191	741	""	{Енглески,Немачки,Шпански,Француски}
55	xEEvsXsvpBNCcP5Df	Женски	5/13/1997	491	""	491	423	""	1345	145	""	""	Медицинска сестра-техничар	""	""	""	4191	491	""	{Мађарски}
56	xEEvsXsvpBNCcP5Df	Женски	4/27/1994	491	""	491	491	741	1335	550	65	1421	Defektolog	""	""	""	1015	491	""	{Енглески}
57	xEEvsXsvpBNCcP5Df	Мушки	10/1/1997	491	""	491	491	""	1335	554	""	""	Šef	""	""	""	3251	491	""	{Енглески,Румунски,Албански}
58	xEEvsXsvpBNCcP5Df	Мушки	7/15/1994	491	""	491	491	100	1335	550	11	334	Master politikologije	""	""	""	4191	491	""	{Енглески,Шведски}
59	xEEvsXsvpBNCcP5Df	Мушки	11/16/1994	741	""	491	491	""	1335	550	""	""	Laboratorijski tehničar	""	""	""	1836	741	""	{Енглески,Немачки}
60	xEEvsXsvpBNCcP5Df	Мушки	2/23/1995	491	""	491	491	741	1333	534	62	1657	Dipl. Inž. Geodezije i geomatike	""	""	""	4191	491	""	{Енглески}
61	xEEvsXsvpBNCcP5Df	Женски	1/31/1990	491	""	491	491	""	1335	550	""	""	Hemijsko-tehnološki tehničar	""	""	""	1748	491	""	{Енглески}
62	xEEvsXsvpBNCcP5Df	Женски	2/12/1994	100	""	491	491	100	1333	536	3	23	Diplomirani logoped	""	""	""	4191	100	""	{Енглески}
63	xEEvsXsvpBNCcP5Df	Мушки	10/1/1990	491	""	491	491	491	1335	555	""	""	Poslodavac	""	""	""	2981	491	""	{Енглески,Немачки}
64	xEEvsXsvpBNCcP5Df	Женски	9/12/1994	741	""	491	423	100	1890	145	86	41	Diplomirani teoretičar umetnosti - muzički pedagog	""	""	""	2556	741	""	{Енглески}
65	xEEvsXsvpBNCcP5Df	Женски	5/13/1997	741	""	738	491	741	1335	553	63	1736	farmaceutski tehničar	""	""	""	4191	741	""	{Енглески,Немачки,Русински}
66	xEEvsXsvpBNCcP5Df	Мушки	12/14/1997	491	""	491	491	100	1334	540	""	""	Tehničar obezbeđenja	""	""	""	1499	491	""	{Енглески}
67	xEEvsXsvpBNCcP5Df	Женски	7/6/1997	100	""	491	491	100	1333	534	3	24	Defektolog-tiflolog	""	""	""	4191	100	""	{Енглески}
68	xEEvsXsvpBNCcP5Df	Женски	9/25/1994	741	""	491	491	741	1334	543	63	1769	Дипломирани филолог српског језика	""	""	""	4191	741	""	{Енглески,Шпански,Македонски}
69	xEEvsXsvpBNCcP5Df	Женски	5/5/1990	741	""	491	491	100	1335	553	12	220	Arheolog	""	""	""	1147	741	""	{Енглески}
70	xEEvsXsvpBNCcP5Df	Мушки	7/4/1998	491	""	491	100	""	1461	145	""	""	Mehatronika	""	""	""	4191	491	""	{Енглески,Руски}
71	xEEvsXsvpBNCcP5Df	Женски	12/12/1994	491	""	491	491	100	1333	534	6	493	magistar farmacije- medicinski biohemicar	""	""	""	4191	491	""	{Енглески}
72	xEEvsXsvpBNCcP5Df	Мушки	8/11/1991	""	Pratteln, CH	491	491	979	1335	556	168	1204	Autoelektričar	""	""	""	2416	""	Pratteln	{Енглески,Немачки}
73	xEEvsXsvpBNCcP5Df	Мушки	7/4/2019	741	""	491	491	741	1335	553	62	1694	Farmaceutski tehničar	""	""	""	4191	741	""	{Енглески}
74	xEEvsXsvpBNCcP5Df	Мушки	7/1/1992	100	""	491	491	491	1334	542	""	""	Komercijalista	""	""	""	1404	100	""	{Енглески}
75	xEEvsXsvpBNCcP5Df	Женски	7/5/1992	491	""	491	491	100	1333	534	22	""	Master matematicar	""	""	""	973	491	""	{Енглески,Шпански}
76	xEEvsXsvpBNCcP5Df	Мушки	8/8/1998	100	""	491	491	""	1334	538	""	""	Finansijski administrator	""	""	""	2556	100	""	{Немачки}
77	xEEvsXsvpBNCcP5Df	Мушки	1/23/1996	741	""	491	491	741	1333	534	62	1651	""	""	""	""	4191	741	""	{Енглески}
78	xEEvsXsvpBNCcP5Df	Мушки	9/12/1989	741	""	491	491	100	1335	555	4	""	ne odnosi se na struku	""	""	""	2647	741	""	{Енглески}
79	xEEvsXsvpBNCcP5Df	Женски	1/14/1990	741	""	491	491	741	1333	534	68	1538	Master molekularni biolog, Doktor genetike biljaka	""	""	""	382	741	""	{Енглески,Немачки,Мађарски,Италијански,Словачки}
80	xEEvsXsvpBNCcP5Df	Женски	9/21/1996	491	""	702	491	""	1335	556	""	""	Tehnicar drumskog saobracaja	""	""	""	3348	491	""	{Енглески}
81	xEEvsXsvpBNCcP5Df	Мушки	2/24/2019	741	""	491	491	491	1335	552	63	1729	Elektrotehničar-računara	""	""	""	4191	741	""	{Енглески,Француски}
82	xEEvsXsvpBNCcP5Df	Мушки	9/25/1996	491	""	491	491	""	1335	553	""	""	Exercise Science	Raleigh, North Carolina (USA)	Shaw University	Exercise Science	2131	491	Raleigh	{Енглески,Немачки,Мађарски}
83	xEEvsXsvpBNCcP5Df	Мушки	1/8/1996	491	""	491	491	""	1335	549	""	""	El.Mex. za termicke i rashladne uredjaje	""	""	""	1745	491	""	{Енглески}
84	xEEvsXsvpBNCcP5Df	Мушки	1/8/1996	491	""	491	491	""	1335	549	""	""	El. Mex. Za termicke i rashladne uredjaje	""	""	""	1745	491	""	{Енглески}
85	xEEvsXsvpBNCcP5Df	Женски	11/18/1997	429	""	429	491	741	1335	557	167	1819	Фармацеутски техничар	""	""	""	4191	429	""	{Енглески,Руски}
86	xEEvsXsvpBNCcP5Df	Женски	6/12/1995	""	Dubai, Ujedinjeni Arapski Emirati	491	491	""	1333	534	""	""	Diplomirani ekonomista	Abu Dhabi, UAE	New York University	Ekonomija	1092	""	Dubai	{Енглески,Италијански}
87	xEEvsXsvpBNCcP5Df	Мушки	6/15/1996	491	""	491	491	100	1335	552	105	""	Elektrotehničar računara	""	""	""	1467	491	""	{Енглески,Немачки}
88	xEEvsXsvpBNCcP5Df	Мушки	10/1/1996	741	""	491	491	741	1335	553	65	1414	Medicinski tehničar	""	""	""	4191	741	""	{Енглески}
89	xEEvsXsvpBNCcP5Df	Женски	12/4/1994	491	""	491	491	741	1333	536	63	1733	Gimnazija	""	""	""	4191	491	""	{Енглески,Руски}
90	xEEvsXsvpBNCcP5Df	Мушки	2/6/1996	491	""	491	491	100	1335	552	5	430	Електротехничар рачунара	""	""	""	4191	491	""	{Енглески,Немачки}
91	xEEvsXsvpBNCcP5Df	Женски	3/25/1998	100	""	714	491	100	1333	534	54	884	Prirodno - matematički smer	""	""	""	4191	100	""	{Енглески,Немачки}
92	xEEvsXsvpBNCcP5Df	Женски	12/5/1995	423	""	702	491	423	1335	547	70	1281	Diplomirao inženjer zaštite životne sredine	""	""	""	4191	423	""	{Енглески}
93	xEEvsXsvpBNCcP5Df	Мушки	7/16/1996	100	""	491	491	""	1333	535	""	""	Inzenjer informacionih tehnologija	""	""	""	1147	100	""	{Енглески}
94	xEEvsXsvpBNCcP5Df	Женски	12/11/1998	491	""	491	491	1014	1334	538	60	1241	Finansijski administrator	""	""	""	4191	491	""	{Енглески,Немачки,Мађарски}
95	xEEvsXsvpBNCcP5Df	Женски	5/5/1998	100	""	491	491	100	1334	539	11	333	Ekonomista	""	""	""	4191	100	""	{Енглески}
96	xEEvsXsvpBNCcP5Df	Мушки	7/26/1996	741	""	491	491	741	1335	548	165	1797	Informatika	""	""	""	2620	741	""	{Енглески}
97	xEEvsXsvpBNCcP5Df	Женски	1/30/1999	38	""	38	491	741	1333	534	62	1664	dipl. inženjer informacionih tehnologija	""	""	""	4191	38	""	{Енглески,Немачки}
98	xEEvsXsvpBNCcP5Df	Женски	2/16/1999	741	""	491	491	741	1333	534	68	1564	priridno matematički smer u gimnaziji	""	""	""	4191	741	""	{Енглески}
99	xEEvsXsvpBNCcP5Df	Мушки	4/7/1998	491	""	491	491	423	1335	551	70	1278	Elektrotehničar Telekomunikacija	""	""	""	4191	491	""	{Енглески}
100	xEEvsXsvpBNCcP5Df	Женски	7/16/1998	100	""	491	491	100	1335	553	""	""	Medicinska sestra tehnicar	""	""	""	4191	100	""	{Енглески,Немачки}
101	xEEvsXsvpBNCcP5Df	Женски	7/25/1996	100	""	491	491	""	1335	557	""	""	Tehničar	""	""	""	2458	100	""	{Енглески,Мађарски}
102	xEEvsXsvpBNCcP5Df	Женски	6/27/1992	491	""	491	491	""	1334	537	""	""	Trgovac	""	""	""	2556	491	""	{Енглески}
103	xEEvsXsvpBNCcP5Df	Женски	8/6/2019	491	""	491	491	1014	1334	538	60	1241	Ekonomista	""	""	""	4191	491	""	{Енглески}
104	xEEvsXsvpBNCcP5Df	Женски	10/28/1996	702	""	702	491	""	1335	545	""	""	Prehrambeni tehnicar	""	""	""	2582	702	""	{Енглески}
105	xEEvsXsvpBNCcP5Df	Мушки	1/31/1995	741	""	491	1103	741	1342	530	71	1521	Tehnicar industrijske farmaceutske technologije	""	""	""	4191	741	""	{Енглески,Мађарски}
106	xEEvsXsvpBNCcP5Df	Женски	4/25/1996	491	""	491	491	741	1334	539	7	1643	dipl. profesor fizickog vaspitanja	""	""	""	4191	491	""	{Енглески}
107	xEEvsXsvpBNCcP5Df	Женски	2/8/1996	491	""	491	491	741	1334	544	63	1728	Poslovni administrator	""	""	""	4191	491	""	{Енглески}
108	xEEvsXsvpBNCcP5Df	Женски	2/11/1996	491	""	491	491	741	1334	544	23	260	Poslovni administrator	""	""	""	4191	491	""	{Енглески}
109	xEEvsXsvpBNCcP5Df	Женски	5/14/2019	491	""	491	491	741	1334	542	63	1744	Србиста	""	""	""	4191	491	""	{Енглески,Руски,Словеначки}
110	xEEvsXsvpBNCcP5Df	Мушки	5/29/1996	741	""	491	491	741	1335	554	68	1555	Masinski tehnicar za kompijutersko konstruisanje	""	""	""	4191	741	""	{Енглески,Немачки}
111	xEEvsXsvpBNCcP5Df	Женски	1/27/1996	491	""	491	491	""	1335	548	""	""	Arhitektonski tehnicar	""	""	""	4191	491	""	{Енглески}
112	xEEvsXsvpBNCcP5Df	Женски	5/13/1996	491	""	491	491	423	1335	545	144	""	Prehrambeni tehničar	""	""	""	4191	491	""	{Енглески}
113	xEEvsXsvpBNCcP5Df	Женски	9/21/1996	741	""	491	491	741	1333	534	65	1416	Magistar farmacije	""	""	""	4191	741	""	{Енглески}
114	xEEvsXsvpBNCcP5Df	Мушки	11/13/1983	491	""	491	491	""	1335	549	""	""	Elektro mehanicar za masine i opremu	""	""	""	1991	491	""	{Енглески}
115	xEEvsXsvpBNCcP5Df	Мушки	1/15/1997	491	""	491	491	""	1335	554	""	""	mašinski tehničar za kompjutersko konstruisanje	""	""	""	1480	491	""	{Енглески}
116	xEEvsXsvpBNCcP5Df	Мушки	7/12/1999	741	""	491	491	741	1335	552	62	1652	Elektrotehničar računara	""	""	""	4191	741	""	{Енглески,Немачки}
117	xEEvsXsvpBNCcP5Df	Женски	7/19/1996	491	""	491	491	741	1335	557	23	260	technicar za biotehnologiju	""	""	""	4191	491	""	{Енглески}
118	xEEvsXsvpBNCcP5Df	Женски	5/10/1996	741	""	491	491	741	1333	536	63	1732	Gimnazijalac	""	""	""	4191	741	""	{Енглески,Француски,Италијански}
119	xEEvsXsvpBNCcP5Df	Мушки	6/20/1996	491	""	491	491	100	1333	534	24	529	Geolog	""	""	""	4191	491	""	{Енглески,Немачки}
120	xEEvsXsvpBNCcP5Df	Женски	1/18/1996	100	""	38	491	100	1333	536	17	182	Gimnazija	""	""	""	4191	100	""	{Енглески,Немачки}
121	xEEvsXsvpBNCcP5Df	Мушки	1/25/1996	741	""	491	491	741	1333	534	62	1712	gimnazija/bez struke	""	""	""	4191	741	""	{Енглески,Италијански}
122	xEEvsXsvpBNCcP5Df	Мушки	10/29/1996	714	""	714	491	""	1335	551	""	""	Elektrotehnicar	""	""	""	1552	714	""	{Енглески}
123	xEEvsXsvpBNCcP5Df	Мушки	12/9/1997	702	""	702	491	""	1334	540	""	""	Tehničar obezbedjenja	""	""	""	3412	702	""	{Енглески}
124	xEEvsXsvpBNCcP5Df	Мушки	4/3/2000	702	""	702	491	""	1335	556	""	""	Tehnicar drumskog saobracaja	""	""	""	3977	702	""	{Енглески}
125	xEEvsXsvpBNCcP5Df	Женски	8/19/1996	100	""	491	491	100	1333	536	10	202	Trenutno završavam FON, te se nadam da ću uskoro imati diplomu, za sad je Gimnazija Dušan Vasiljev	""	""	""	4191	100	""	{Енглески,Немачки}
126	xEEvsXsvpBNCcP5Df	Мушки	6/6/1996	100	""	491	100	""	1457	145	""	""	Programer	""	""	""	1147	100	""	{Енглески,Мађарски}
127	xEEvsXsvpBNCcP5Df	Мушки	3/14/1995	741	""	491	491	100	1334	538	157	287	Diplomirani ekonomista	""	""	""	1050	741	""	{Енглески}
128	xEEvsXsvpBNCcP5Df	Женски	2/13/1996	100	""	491	491	100	1333	534	10	199	Diplomirani inzenjer organizacionih nauka	""	""	""	1147	100	""	{Енглески}
129	xEEvsXsvpBNCcP5Df	Женски	1/26/1999	100	""	491	491	100	1333	534	11	339	Studiram	""	""	""	4191	100	""	{Енглески}
130	xEEvsXsvpBNCcP5Df	Женски	12/17/2000	738	""	738	491	""	1335	556	""	""	Tehničar drumskog saobraćaja	""	""	""	4191	738	""	{Незапослен}
131	xEEvsXsvpBNCcP5Df	Мушки	5/17/1995	491	""	491	491	""	1335	551	""	""	Elektrotehničar telekomunikacija	""	""	""	1748	491	""	{Енглески}
132	xEEvsXsvpBNCcP5Df	Мушки	12/3/1996	491	""	491	491	741	1333	536	62	1653	Inženjer elektrotehnike	""	""	""	1147	491	""	{Енглески,Немачки}
133	xEEvsXsvpBNCcP5Df	Мушки	1/17/1997	738	""	738	491	741	1335	556	63	1740	Elektrotehnicar na vozilima	""	""	""	4191	738	""	{Енглески}
134	xEEvsXsvpBNCcP5Df	Женски	9/24/1994	1014	""	491	1014	""	1333	535	""	""	Informaricki smer	""	""	""	2582	1014	""	{Енглески}
135	xEEvsXsvpBNCcP5Df	Женски	12/29/1992	491	""	491	491	491	1335	554	""	""	Masinski tehnicara za kompijutersko konstruisanje	""	""	""	1748	491	""	{Енглески}
136	xEEvsXsvpBNCcP5Df	Мушки	2/19/1992	100	""	491	491	100	1334	543	16	378	Turisticki tehnicar	""	""	""	2390	100	""	{Енглески,Шпански}
137	xEEvsXsvpBNCcP5Df	Мушки	11/19/1992	491	""	491	491	741	1333	536	7	1643	Master sporta i fizičkog vaspitanja	""	""	""	2121	491	""	{Енглески}
138	xEEvsXsvpBNCcP5Df	Женски	7/20/1996	491	""	491	741	741	1412	134	66	1433	Tehničar tekstilnog dizajna	""	""	""	4191	491	""	{Енглески}
139	xEEvsXsvpBNCcP5Df	Мушки	2/1/1990	741	""	491	491	741	1334	539	60	1237	Diplomirani ekonomista	""	""	""	1934	741	""	{Енглески}
140	xEEvsXsvpBNCcP5Df	Мушки	9/19/1993	491	""	491	491	""	1335	549	""	""	Elwktromehanicar	""	""	""	3190	491	""	{Енглески}
141	xEEvsXsvpBNCcP5Df	Мушки	1/9/1992	491	""	491	491	""	1335	552	""	""	Elektrotehnicar Elektornike	""	""	""	2247	491	""	{Енглески,Немачки}
142	xEEvsXsvpBNCcP5Df	Женски	1/16/1994	491	""	491	491	741	1333	536	63	1744	Diplomirani filolog srbista	""	""	""	4191	491	""	{Енглески}
143	xEEvsXsvpBNCcP5Df	Женски	1/16/1994	491	""	491	491	741	1333	536	63	1744	Diplomirani filolog srbista	""	""	""	4191	491	""	{Енглески}
144	xEEvsXsvpBNCcP5Df	Женски	5/15/1989	741	""	491	491	741	1333	534	68	1574	master matematicar primenjene matematike	""	""	""	2318	741	""	{Енглески}
145	xEEvsXsvpBNCcP5Df	Мушки	6/21/1995	491	""	491	491	741	1335	551	66	1430	Elektrotehničar telekomunikacuje	""	""	""	2935	491	""	{Енглески}
146	xEEvsXsvpBNCcP5Df	Мушки	7/7/1995	741	""	491	491	741	1335	547	""	""	Техничар за заштиту животне средине	""	""	""	4191	741	""	{Енглески}
147	xEEvsXsvpBNCcP5Df	Женски	12/23/1995	741	""	491	491	741	1334	539	62	1666	Ekonomski tehnicar	""	""	""	4191	741	""	{Енглески}
148	xEEvsXsvpBNCcP5Df	Женски	8/27/1995	100	""	491	491	741	1334	539	63	1737	Diplomirani psiholog	""	""	""	4191	100	""	{Енглески,Италијански}
149	xEEvsXsvpBNCcP5Df	Мушки	5/3/1995	491	""	491	491	741	1335	552	142	""	Inženjer računara i informatike	""	""	""	1147	491	""	{Енглески}
150	xEEvsXsvpBNCcP5Df	Мушки	6/16/1995	741	""	491	491	741	1335	548	62	1712	Arhitektonski tehničar	""	""	""	4191	741	""	{Енглески}
151	xEEvsXsvpBNCcP5Df	Мушки	10/15/1996	741	""	491	741	741	1398	129	62	1650	Dipl. Inženjer Elektrotehnike	""	""	""	4191	741	""	{Енглески}
\.

ALTER SEQUENCE koshnica_user_id_seq RESTART WITH 187;
ANALYZE koshnica_user;
