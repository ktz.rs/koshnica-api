create or replace view cities as
select id, name, postal_code from data.city;

create or replace view high_schools as
select id, name || ' - ' || city as name, city from data.high_school;

create or replace view high_school_majors as
select id, name, school_id from data.high_school_major;

create or replace view universities as
select id, name, city_id, created_on, updated_on from data.university;

create or replace view faculties as 
select id, name, university_id from data.faculty;

create or replace view faculty_majors as
select id, name, faculty_id from data.faculty_major;

create or replace view qualifications as
select id, q_id, name from data.qualification;

create or replace view koshnica_users as
select id, user_hash, gender, birth_date, city, abroad_city, primary_school_city, high_school_city, faculty_city, high_school, high_school_major, faculty, faculty_major, working_profession, abroad_faculty_city, abroad_faculty, abroad_faculty_major, qualification, working_city, abroad_working_city, languages from data.koshnica_user;

create or replace view school_emigration as
select name, COUNT (name) as value
from data.koshnica_user 
INNER JOIN data.city
on data.city.id = data.koshnica_user.faculty_city
where faculty_city != high_school_city or faculty_city is null
group by name
order by COUNT (name) desc
LIMIT 4;

create or replace view job_emigration as
select name, COUNT (name) as value
from data.koshnica_user 
INNER JOIN data.city
on data.city.id = data.koshnica_user.working_city
where primary_school_city != working_city or working_city is null
group by name
order by COUNT (name) desc
LIMIT 4;

create or replace view languages as
with languages as (
  select unnest(languages) as language from data.koshnica_user ) 
select language as name, count(language) as value
from languages
group by language
order by count(language) desc;

create or replace view qualification_statistics as
select name, count (name) as value
from data.koshnica_user
inner join data.qualification
on data.qualification.id = data.koshnica_user.qualification
group by name
order by COUNT (name) desc;

create or replace view school_statistics as
select name || ' - ' || data.high_school.city as name, count (name) as value
from data.koshnica_user
inner join data.high_school
on data.high_school.id = data.koshnica_user.high_school
group by name || ' - ' || data.high_school.city
order by COUNT (name) desc;

create or replace view emigration_from_kikinda_abroad as
select abroad_city as name, COUNT (abroad_city) as value
from data.koshnica_user 
where (primary_school_city = 491 or high_school_city = 491) and city is null
group by abroad_city
order by COUNT (abroad_city) desc;

create or replace view emigration_from_kikinda as
select name, count (name) as value
from data.koshnica_user
INNER JOIN data.city
on data.city.id = data.koshnica_user.city
where (primary_school_city = 491 or high_school_city = 491) and city != 491
group by name
order by COUNT (name) desc;

create or replace view working_cities as
select name, count (name) as value
from data.koshnica_user
INNER JOIN data.city
on data.city.id = data.koshnica_user.working_city
where working_city is not null
group by name
order by COUNT (name) desc;

create or replace view abroad_working_cities as
select abroad_working_city as name, count (abroad_working_city) as value
from data.koshnica_user
where abroad_working_city is not null
group by abroad_working_city
order by COUNT (abroad_working_city) desc;

create or replace view returning_cities as
select name, count (name) as value
from data.koshnica_user
INNER JOIN data.city
on data.city.id = data.koshnica_user.city
where primary_school_city = city and (primary_school_city != high_school_city or primary_school_city != faculty_city)
group by name
order by COUNT (name) desc;
