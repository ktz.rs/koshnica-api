create table city (
	id	serial primary key,
	name	text not null,
	postal_code	int
);

create table high_school
(
  id serial primary key,
  name text not null,
  city text not null
);

create table high_school_major (
  id  serial primary key,
  name text not null,
  school_id int null references high_school(id)
);
create index high_school_major_school_id_index on high_school_major(school_id);

create table university (
	id	serial primary key,
	name	text not null,
	city_id	int null references city(id),
	created_on	timestamptz not null default now(),
	updated_on	timestamptz
);
create index university_city_id_index on city(id);

create table faculty (
	id	serial primary key,
	name	text not null,
	university_id int null references university(id)
);
create index faculty_university_id_index on faculty(university_id);

create table faculty_major (
	id serial primary key,
	name text not null,
	faculty_id int null references faculty(id)
);
create index faculty_major_faculty_id_index on faculty(id);

create table qualification (
	id serial primary key,
	q_id decimal not null,
	name text not null
);
create index qualifications_id_index on qualification(id);

create table if not exists koshnica_user (
	id serial primary key,
	user_hash text not null,
	gender text not null,
	birth_date timestamptz not null,
	city int null references city(id),
	abroad_city text null,
	primary_school_city int null references city(id),
	high_school_city int null references city(id),
	faculty_city int null references city(id),
	high_school int null references high_school(id),
	high_school_major int null references high_school_major(id),
	faculty int null references faculty(id),
	faculty_major int null references faculty_major(id),
	working_profession text null,
	abroad_faculty_city text null,
	abroad_faculty text null,
	abroad_faculty_major text null,
	qualification int null references qualification(id),
	working_city int null references city(id),
	abroad_working_city text null,
	languages text []
);
create index koshnica_users_id_index on koshnica_user(id);
